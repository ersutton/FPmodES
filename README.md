# Summary

This Functional Programming Library will probably never be used in a production environment, but will instead be used as a learning tool for myself as a means of getting a better handle on functional programming techniques.

This Library will be heavily inspired by https://github.com/getify/Functional-Light-JS

# Specification

Write a unit tested, typescript interfaced isomorphic JS module that I can use in my personal projects.
