import { asyncPipe, partial, partialRight } from '../../src';

describe('async functions tests', () => {
  test('test asyncPipe', () => {
    const valueReturner: (value: number) => Promise<number> = (value: number) =>
      Promise.resolve(value);
    const valueDoubler: (value: number) => Promise<number> = (value: number) =>
      Promise.resolve(value * 2);
    const weirdpromise = asyncPipe(valueReturner, valueDoubler);
    expect(weirdpromise(4).then(value => expect(value).toEqual(8)));
  });

  test('test asyncPipe with partial...', () => {
    const valueReturner: (value: number) => Promise<number> = (
      value: number
    ) => {
      return Promise.resolve(value);
    };

    const valueDoubler: (value: number) => Promise<number> = (
      value: number
    ) => {
      return Promise.resolve(value * 2);
    };
    const valueAdder: (value: number) => Promise<number> = (value: number) => {
      return Promise.resolve(value + 1);
    };
    const weirdpromise = asyncPipe(
      valueReturner,
      valueDoubler,
      valueAdder,
      valueAdder,
      valueDoubler
    );
    const partiallyAppliedweirdpromise = partial(
      partialRight(asyncPipe, valueDoubler, valueAdder),
      valueReturner,
      valueDoubler
    );
    const weirdPromise2 = partiallyAppliedweirdpromise(valueAdder);
    expect(
      weirdpromise(4).then((value: {} | undefined) =>
        expect((value as unknown) as number).toEqual(20)
      )
    );
    expect(
      weirdPromise2(4).then((value: {} | undefined) =>
        expect((value as unknown) as number).toEqual(20)
      )
    );
  });
});
