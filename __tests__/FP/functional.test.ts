import {
  unary,
  identity,
  constant,
  apply,
  unapply,
  partial,
  reverseArgs,
  partialRight,
  curry,
  pipe,
  compose,
} from '../../src/FP/index';

describe('unary function tests', () => {
  test('test that without unary we have issues with mappingwith parseInt', () => {
    expect(['1', '2', '3'].map(parseInt)).toEqual([1, NaN, NaN]);
  });
  test('test that unary saves the day', () => {
    expect(['1', '2', '3'].map(unary(parseInt))).toEqual([1, 2, 3]);
  });
});

describe('identity function tests', () => {
  const words = '   Now is the time for all...  '.split(/\s|\b/);
  test('test filtering with the unity function clears spaces', () => {
    expect(words.filter(identity)).toEqual([
      'Now',
      'is',
      'the',
      'time',
      'for',
      'all',
      '...',
    ]);
  });
});

describe('constant function tests', () => {
  test('test that JS promises work correctly using this little utilty function', done => {
    const promise = new Promise(resolve => {
      resolve(undefined);
    });
    promise.then(constant('hello')).then((value: string) => {
      value = value;
      expect(value).toEqual('hello');
      done();
    });
  });
});

describe('spreadArgs/apply function tests', () => {
  test('test that our spreadArgs/apply function correctly converts inputs from (x,y) to ([x,y])', () => {
    const testFunction = function(x: number, y: number): number {
      return x + y;
    };
    expect(apply(testFunction)([5, 3])).toEqual(8);
  });
});

describe('gatherArgs/unapply function tests', () => {
  test('test that our gatherArgs/unapply function correctly converts inputs from ([x,y]) to (x,y)', () => {
    const testFunction = function([x, y]: [number, number]): number {
      return x + y;
    };
    expect(unapply(testFunction)(5, 3)).toEqual(8);
  });
});

describe('partial function tests', () => {
  test('test that we can reduce the arity of some generic functions', () => {
    const testFunction = function(x: number, y: number, z: number): number {
      return (x + y) * z;
    };
    expect(partial(testFunction, 1, 2)(3)).toEqual(9);
    expect([1, 2, 3, 4, 5].map(partial(testFunction, 1, 2))).toEqual([
      3,
      6,
      9,
      12,
      15,
    ]);
  });
});

describe('reverse args function tests', () => {
  test('test that we can reverse function parameters', () => {
    const divide = function divide(dividend: number, divisor: number): number {
      return dividend / divisor;
    };
    expect(divide(4, 2)).toEqual(2);
    expect(reverseArgs(divide)(2, 4)).toEqual(2);
    // lets now partially apply the divisor...
    expect(partial(reverseArgs(divide), 2)(4)).toEqual(2);
  });
});

describe('partialRight function tests', () => {
  test('test that we can reverse function parameters and partially apply them', () => {
    const divide = function divide(
      dividend: number,
      adder: number,
      divisor: number
    ): number {
      return dividend / divisor + adder;
    };
    expect(partialRight(divide, 2, 1)(4)).toEqual(3);
    //expect(partialRight(divide, 1, 2)(4)).toEqual(3);
  });
});
describe('curry function tests', () => {
  test('test that checks currying works', () => {
    const divide = function divide(dividend: number, divisor: number): number {
      return dividend / divisor;
    };
    expect(curry(divide)(4)(2)).toEqual(2);

    const divideWithArgsReversed = reverseArgs(divide);
    const curriedRevered = curry(divideWithArgsReversed, 2)(2);
    expect([2, 4, 6, 8, 10].map(curriedRevered)).toEqual([1, 2, 3, 4, 5]);
  });
});

describe('compose and pipe tests', () => {
  test('test that pipe works and compose works', () => {
    const divide = function divide(dividend: number, divisor: number): number {
      return dividend / divisor;
    };

    const adder = function adder(value: number, adder: number): number {
      return value + adder;
    };

    const addTen = partialRight(adder, 10);
    const divideByTwo = partialRight(divide, 2);

    const add10ThenDivideByTwoPiped = pipe(
      addTen,
      divideByTwo
    );
    const add10ThenDivideByTwoCompose = compose(
      divideByTwo,
      addTen
    );

    expect(add10ThenDivideByTwoCompose(2)).toEqual(6);
    expect(add10ThenDivideByTwoPiped(2)).toEqual(6);
  });
});
