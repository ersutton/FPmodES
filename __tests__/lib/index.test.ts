import {
  deepSetProp,
  deepGetProp,
  getProp,
  setProp,
  objectKeyMerger,
  objectKeysMerger,
  promisify,
  asyncPipe,
  partial,
  stripKeyFromObject,
  stripKeyFromObjects,
  isStringAndNotEmpty,
  TimeHelper,
} from '../../src/index';
import { emailCheck, chunkArray } from '../../src/Lib';

describe('deepSetProp tests', () => {
  test('test deepSetProp works', () => {
    const myObject = {
      tier1: {
        tier2: {
          value: 5,
        },
      },
    };

    const newObj = deepSetProp(myObject, ['tier1', 'tier2', 'value'], 12);
    expect(myObject.tier1.tier2.value).toEqual(12);
    expect(newObj).toEqual(myObject);
  });
});

describe('deepGetProp tests', () => {
  test('test deepGetProp works', () => {
    const myObject = {
      tier1: {
        tier2: {
          value: 5,
        },
      },
    };
    expect(deepGetProp(myObject, 'tier1', 'tier2', 'value')).toEqual(5);
  });
});

describe('getProp and setProp tests', () => {
  test('test getProp works', () => {
    const myObject = {
      tier1: 5,
    };
    expect(getProp('tier1', myObject)).toEqual(5);

    const mySecondObj = Object.assign({}, myObject);
    setProp('tier1', mySecondObj, 10);
    expect(getProp('tier1', mySecondObj)).toEqual(10);
  });
});

describe('objectkeyMerger tests', () => {
  test('test that objects merge correctly', () => {
    const myObject = {
      value1: 5,
      value2: 10,
    };
    const secondObject = {
      value1: 10,
      value2: 20,
    };

    const newThirdObject = objectKeyMerger(myObject, secondObject, 'value1');
    expect(newThirdObject).toEqual({ value1: 10, value2: 10 });
    expect(myObject).toEqual({ value1: 5, value2: 10 });
    expect(secondObject).toEqual({ value1: 10, value2: 20 });
  });
});

describe('objectkeyMergers tests', () => {
  test('test that objects merge correctly with multiple keys', () => {
    const myObject = {
      value1: 5,
      value2: 10,
    };
    const secondObject = {
      value1: 10,
      value2: 20,
    };

    const newThirdObject = objectKeysMerger(
      myObject,
      secondObject,
      'value1',
      'value2'
    );
    expect(newThirdObject).toEqual({ value1: 10, value2: 20 });
    expect(myObject).toEqual({ value1: 5, value2: 10 });
    expect(secondObject).toEqual({ value1: 10, value2: 20 });
    const newFourthObject = objectKeysMerger(myObject, secondObject, 'value1');
    expect(newFourthObject).toEqual({ value1: 10, value2: 10 });
  });
});

describe('promisify tests', () => {
  test('test that checks that a function that is passed in becomes a promise...', () => {
    const adder = function(value1: number, value2: number) {
      return value1 + value2;
    };
    const multiplier = function(value1: number, value2: number) {
      return value1 * value2;
    };

    const addOne = partial(adder, 1);
    const multiplyByTwo = partial(multiplier, 2);

    const promiseAdder2 = promisify(addOne);
    const promiseMultiplier2 = promisify(multiplyByTwo);

    asyncPipe(promiseAdder2, promiseMultiplier2)(1).then(data =>
      expect(data).toEqual(4)
    );
  });
});

describe('stripKeyFromObject tests', () => {
  test('test that key is removed from object successfully', () => {
    const testObject = {
      value: 1,
      value2: 2,
      valueNested: {
        value3: 3,
      },
    };

    const strippedObject = stripKeyFromObject('value', testObject);
    expect(testObject).toEqual({
      value: 1,
      value2: 2,
      valueNested: {
        value3: 3,
      },
    });
    expect(strippedObject).toEqual({
      value2: 2,
      valueNested: {
        value3: 3,
      },
    });
    const strippingSomethingThatDoesntExist = stripKeyFromObject(
      'notAKey',
      testObject
    );
    expect(testObject).toEqual({
      value: 1,
      value2: 2,
      valueNested: {
        value3: 3,
      },
    });
    expect(strippingSomethingThatDoesntExist).toEqual({
      value: 1,
      value2: 2,
      valueNested: {
        value3: 3,
      },
    });
  });
});
describe('stripKeyFromObjects tests', () => {
  test('test that key is removed from objects successfully', () => {
    const testObject = {
      value: 1,
      value2: 2,
      valueNested: {
        value3: 3,
      },
    };
    const secondObject = {
      value: 1,
      value2: 2,
      valueNested: {
        value3: 3,
      },
    };

    const arrayObj = [testObject, secondObject];
    const strippedArray = stripKeyFromObjects('value', arrayObj);

    expect(strippedArray).toEqual([
      {
        value2: 2,
        valueNested: {
          value3: 3,
        },
      },
      {
        value2: 2,
        valueNested: {
          value3: 3,
        },
      },
    ]);
  });
});

describe('isStringAndNotEmptyTests', () => {
  test('test that isStringAndNotEmptyWorks', () => {
    expect(isStringAndNotEmpty('t')).toBe(true);
    expect(isStringAndNotEmpty('')).toBe(false);
    expect(isStringAndNotEmpty((undefined as unknown) as string)).toBe(false);
  });
});

describe('timeHelperTests', () => {
  test('test that TimeHelper works', () => {
    const timeHelper = TimeHelper();
    const timeHelper2 = TimeHelper();
    // test that singleton pattern is working
    expect(timeHelper).toBe(timeHelper2);
    expect(timeHelper.daysInMilliSeconds(2)).toEqual(1000 * 60 * 60 * 24 * 2);
    expect(timeHelper.hoursInMilliSeconds(2)).toEqual(1000 * 60 * 60 * 2);
    expect(timeHelper.minutesInMilliSeconds(2)).toEqual(1000 * 60 * 2);
    expect(timeHelper.monthsInMilliSeconds(2)).toEqual(
      1000 * 60 * 60 * 24 * 30 * 2
    );
    expect(timeHelper.secondsInMilliSeconds(2)).toEqual(1000 * 2);
  });
});

describe('emailCheck tests', () => {
  test('testing our emailCheck function', () => {
    expect(emailCheck('ersutton88@gmail.com')).toBeTruthy();
    expect(emailCheck('ersutton88@gmail.com;ersutton88@gmail.com')).toBeFalsy();
    expect(
      emailCheck('ersutton88@gmail.com; ersutton88@gmail.com')
    ).toBeFalsy();
    expect(emailCheck('ersutton88@gmail.com ersutton88@gmail.com')).toBeFalsy();
  });
});

// describe('playing with ducks', () => {
//   test('ducks?', () => {
//     const duck = duckStamp();
//     expect(
//       duck
//         .fly()
//         .land()
//         .isFlying()
//     ).toEqual(false);
//     //   createDuck({})
//     //     .fly()
//     //     .isFlying()
//     // ).toBe(true);
//   });
// });

// describe('fun with stamps', () => {
//   test('ducks?', () => {
//     const duck = duckStamp();
//     expect(
//       duck
//         .fly()
//         .land()
//         .isFlying()
//     ).toEqual(false);
//     //   createDuck({})
//     //     .fly()
//     //     .isFlying()
//     // ).toBe(true);
//   });
// });

describe('chunk array test', () => {
  test('test Chunk', () => {
    expect(chunkArray<number>([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 2)).toEqual([
      [1, 2],
      [3, 4],
      [5, 6],
      [7, 8],
      [9, 10],
    ]);
  });
});
