/**
 * async functions untested.
 */
/**
 * a promise based pipe. instead of writing things like this:
 * something.then(somethingelse).then(somethingElseAgain)
 *
 * you can write it like this: asyncPiped(something,somethingElse,somethingElseAgain);
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(result: any) => Promise<any>}
 */
export declare function asyncPipe<ReturnType, ReturnMethodArgType>(...fns: ((value?: any) => Promise<any>)[]): (result: ReturnMethodArgType) => Promise<ReturnType | undefined>;
/**
 * a promise based compose. instead of writing things like this:
 * something.then(somethingelse).then(somethingElseAgain)
 *
 * you can write it like this: asyncPiped(somethingElseAgain,somethingelse,something);
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(result: any) => Promise<any>}
 */
export declare const asyncCompose: (result: any) => Promise<any>;
/**
 * excute an array of promise factories in sequential order
 * stolen from here: https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
 *
 * @export
 * @param {(() => Promise<any>)[]} promiseFactories
 * @returns {Promise<any>}
 */
export declare function excecuteSequentially(promiseFactories: (() => Promise<any>)[]): Promise<any>;
