"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var functional_1 = require("./functional");
/**
 * async functions untested.
 */
/**
 * a promise based pipe. instead of writing things like this:
 * something.then(somethingelse).then(somethingElseAgain)
 *
 * you can write it like this: asyncPiped(something,somethingElse,somethingElseAgain);
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(result: any) => Promise<any>}
 */
function asyncPipe() {
    var fns = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        fns[_i] = arguments[_i];
    }
    return function asyncPiped(result) {
        // need to take a copy of the functions else the slice inside will ruin it and it will only run once
        var copiedFunctions = fns.slice();
        function asyncPiper(result) {
            if (copiedFunctions.length > 0) {
                return copiedFunctions.shift()(result).then(asyncPiper);
            }
            return result;
        }
        return asyncPiper(result);
    };
}
exports.asyncPipe = asyncPipe;
/**
 * a promise based compose. instead of writing things like this:
 * something.then(somethingelse).then(somethingElseAgain)
 *
 * you can write it like this: asyncPiped(somethingElseAgain,somethingelse,something);
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(result: any) => Promise<any>}
 */
exports.asyncCompose = functional_1.reverseArgs(asyncPipe);
/**
 * excute an array of promise factories in sequential order
 * stolen from here: https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
 *
 * @export
 * @param {(() => Promise<any>)[]} promiseFactories
 * @returns {Promise<any>}
 */
function excecuteSequentially(promiseFactories) {
    var result = Promise.resolve();
    promiseFactories.forEach(function (promiseFactory) {
        result = result.then(promiseFactory);
    });
    return result;
}
exports.excecuteSequentially = excecuteSequentially;
//# sourceMappingURL=asyncFunctional.js.map