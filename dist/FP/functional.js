"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Library that borrows heavily from https://github.com/getify/Functional-Light-JS
 *
 * Reason for this library is for my own learning outcomes - get to play in Functional Programming land in the hope to use some of these techniques in real projects
 *
 * Typescript Interface Guide
 * I have added optional interface parameters so that if you want, you can use this library with typescript and have fairly indepth typechecking
 * however, by adding these interfaces we are telling the TS compiler if a number is passed in as a parameter, a number is expected as the return type. we will get compiler warnings if we misuse these functions. great stuff
 *
 * RT = ReturnType - This is the Return Values expected type
 * AT = ArgumentType - this is an arguments type
 * T = Generic Type - may be something that is both passed in and returned
 */
/**
 * First function in https://github.com/getify/Functional-Light-JS/blob/master/ch3.md
 * used to ensure that a function can only ever receive a single argument, an arity of 1.
 *
 * Consider:
 * ["1","2","3"].map( parseInt );
 * would result in:
 * [1,NaN,NaN]
 *
 * this is due to parseInts signature is:
 * parseInt(str,radix) radix is its base (like base 2 or base 10), see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
 *
 * whereas with this simple unary function we can do this:
 * ["1","2","3"].map( unary( parseInt ) );
 * [1,2,3]
 *
 * @export
 * @template RT
 * @template AT
 * @param {Function} fn
 * @returns {(arg: AT) => RT}
 */
function unary(fn) {
    return function onlyOneArg(arg) {
        return fn(arg);
    };
}
exports.unary = unary;
/**
 * A seemingly pointless function from https://github.com/getify/Functional-Light-JS/blob/master/ch3.md
 * all it does is return the value passed into it. This is useful for filtering arrays with empty values:
 *
 * var words = "   Now is the time for all...  ".split( /\s|\b/ );
 * words;
 * ["","Now","is","the","time","for","all","...",""]
 *
 * words.filter( identity );
 * ["Now","is","the","time","for","all","..."]
 *
 * @export
 * @template T
 * @param {T} value
 * @returns {T}
 */
function identity(value) {
    return value;
}
exports.identity = identity;
/**
 * Certain APIs don't let you pass a value directly into a method,
 * but require you to pass in a function, even if that function literally just returns the value.
 * One such API is the then(..) method on JS Promises:
 *
 * With this tidy little FP utility, we can solve our then(..) annoyance properly:
 * p1.then( foo ).then( constant( p2 ) ).then( bar );
 *
 * @export
 * @template T
 * @param {T} v
 * @returns {() => T}
 */
function constant(v) {
    return function value() {
        return v;
    };
}
exports.constant = constant;
/**
 * function that is used to bend function signatures to our will.
 * we can pass in a function we want to change the parameters for and it will return a function that will play nice.
 * if we have a function that looks like this:
 * myFunction(x,y)
 * and we want it to behave like this:
 * myFunction([x,y])
 * we can use this function to convert it
 *
 * @export
 * @param {Function} fn
 * @returns {*}
 */
var spreadArgs = function spreadArgs(fn) {
    return function spreadFn(argsArr) {
        return fn.apply(void 0, argsArr);
    };
};
/**
 * see spreadArgs above
 *
 * @param {Function} fn
 * @returns {*}
 */
exports.apply = spreadArgs;
/**
 * function that is used to bend function signatures to our will.
 * we can pass in a function we want to change the parameters for and it will return a function that will play nice.
 * if we have a function that looks like this:
 * myFunction([x,y])
 * and we want it to behave like this:
 * myFunction(x,y)
 * we can use this function to convert it
 *
 * @export
 * @param {Function} fn
 * @returns {*}
 */
exports.gatherArgs = function gatherArgs(fn) {
    return function gatheredFn() {
        var argsArr = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            argsArr[_i] = arguments[_i];
        }
        return fn(argsArr);
    };
};
/**
 * see gatherArgs above
 *
 * @param {Function} fn
 * @returns {*}
 */
exports.unapply = exports.gatherArgs;
/**
 * Function that is used to partially apply function parameters from left to right.
 * So lets say we have a function (x,y,z) => any, but we know ahead of time what x and y are,
 * and we want instead to have a function that is used at the call site that is (z) => any. going from an arity of 3 to 1. shiny.0
 * this function you could call like this:
 * partial(fn, x,y) => reducedArityFn(z) => any
 *
 * @export
 * @param {Function} fn
 * @param {...any[]} presetArgs
 * @returns {(...laterArgs: any[]) => any}
 */
function partial(fn) {
    var presetArgs = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        presetArgs[_i - 1] = arguments[_i];
    }
    return function partiallyApplied() {
        var laterArgs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            laterArgs[_i] = arguments[_i];
        }
        return fn.apply(void 0, presetArgs.concat(laterArgs));
    };
}
exports.partial = partial;
/**
 * function that allows you to pass in a function and get a new function back with the args reversed. useful if we want to use partial above and set the final parameters.
 *
 * @export
 * @param {Function} fn
 * @returns {(...args: any[]) => any}
 */
function reverseArgs(fn) {
    return function argsReversed() {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return fn.apply(void 0, args.reverse());
    };
}
exports.reverseArgs = reverseArgs;
/**
 * function that allows us to partially apply function parameters from right to left.
 * to achieve this we first have to reverse the args in the passed in function. we then partially apply this
 * newly reverse args'd function with the passed in partial parameters array. we then reverse taht back.
 * this returns a function that behaves like the originally passed in function, only with reduced arity.
 *
 * @export
 * @param {Function} fn
 * @param {...any[]} presetArgs
 * @returns {(...args: any[]) => any}
 */
function partialRight(fn) {
    var presetArgs = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        presetArgs[_i - 1] = arguments[_i];
    }
    return reverseArgs(partial.apply(void 0, [reverseArgs(fn)].concat(presetArgs)));
}
exports.partialRight = partialRight;
/**
 * currys a function. returns a function that takes the next argument, unless it is the last
 * argument, in which case it calls the original function with all the parameters.
 *
 * @export
 * @param {Function} fn
 * @param {number} [arity=fn.length]
 * @returns {ICurry}
 */
function curry(fn, arity) {
    if (arity === void 0) { arity = fn.length; }
    // when first called this IIFE returns the curried version of the function, intiating the args array to be an empty array.
    return (function nextCurried(prevArgs) {
        /*
          this is the function that is affectively returned when 'curry' is first called, with prevArgs set to [].
          every successive call will build up this array until the arity is met
        */
        return function curried(nextArg) {
            var args = prevArgs.concat([nextArg]);
            if (args.length >= arity) {
                return fn.apply(void 0, args);
            }
            else {
                return nextCurried(args);
            }
        };
    })([]);
}
exports.curry = curry;
/**
 * this allows the composition of functions with the order of execution from left to right
 *
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(val: any) => any}
 */
function pipe() {
    var fns = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        fns[_i] = arguments[_i];
    }
    return function composed(parameterToPassToFirstFunction) {
        return fns.reduce(function (result, fn, index, array) {
            return fn(result);
        }, parameterToPassToFirstFunction);
    };
}
exports.pipe = pipe;
/**
 * this allows the composition of functions with the order of execution from right to left
 *
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(val: any) => any}
 */
exports.compose = reverseArgs(pipe);
/**
 * gets the value of the object at the specified key
 *
 * @export
 * @param {string} key
 * @param {*} object
 * @returns {*}
 */
function getProp(key, object) {
    return object[key];
}
exports.getProp = getProp;
/**
 * sets the passed in object's value at the key specified. returns the object. Pure Version
 * for impure version use setProp
 * @param name
 * @param object
 * @param value
 */
function setPropPure(key, object, value) {
    if (object !== undefined) {
        var newObj = Object.assign({}, object);
        return setProp(key, newObj, value);
    }
    return object;
}
exports.setPropPure = setPropPure;
/**
 * impure setProp - if you want a pure version use setPropPure
 *
 * @export
 * @param {string} key
 * @param {*} object
 * @param {*} value
 * @returns {*}
 */
function setProp(key, object, value) {
    if (object !== undefined)
        object[key] = value;
    return object;
}
exports.setProp = setProp;
/**
 * creates a new object with the named key and a value assigned to it.
 *
 * @export
 * @param {string} key
 * @param {*} value
 * @returns
 */
function makeObjProp(key, value) {
    return setProp(key, {}, value);
}
exports.makeObjProp = makeObjProp;
//# sourceMappingURL=functional.js.map