/**
 * really small timehelper, would rather use this than the huge time libraries like moment
 */
export interface ITimeHelper {
    secondsInMilliSeconds: (seconds: number) => number;
    minutesInMilliSeconds: (minutes: number) => number;
    hoursInMilliSeconds: (hours: number) => number;
    daysInMilliSeconds: (days: number) => number;
    monthsInMilliSeconds: (months: number) => number;
}
export interface ITimeHelperSingleton {
    (): ITimeHelper;
}
export declare const TimeHelper: ITimeHelperSingleton;
