"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
exports.TimeHelper = (function () {
    var timeHelper;
    function createInstance() {
        var millisecondsInASecond = 1000;
        var millisecondsInAMinute = multiply(millisecondsInASecond, 60);
        var milliSecondsInAnHour = multiply(millisecondsInAMinute, 60);
        var milliSecondsInADay = multiply(milliSecondsInAnHour, 24);
        var milliSecondsInAMonth = multiply(milliSecondsInADay, 30);
        function multiply(number1, number2) {
            return number1 * number2;
        }
        return {
            secondsInMilliSeconds: __1.partial(multiply, millisecondsInASecond),
            minutesInMilliSeconds: __1.partial(multiply, millisecondsInAMinute),
            hoursInMilliSeconds: __1.partial(multiply, milliSecondsInAnHour),
            daysInMilliSeconds: __1.partial(multiply, milliSecondsInADay),
            monthsInMilliSeconds: __1.partial(multiply, milliSecondsInAMonth),
        };
    }
    return function getInstance() {
        if (timeHelper === undefined) {
            timeHelper = createInstance();
        }
        return timeHelper;
    };
})();
//# sourceMappingURL=dateHelper.js.map