"use strict";
// fairly overkill, just needed a simple object for holding exit state
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * simple object for storing exiting state. as it is an object that
 * can be passed into several places, all can monitor the exiting state
 * because it is part of an object. mutating it in one place mutates it everywhere (pass by reference)
 *
 * @returns {IExitingObj}
 */
function exitingFactory() {
    var exiting = false;
    return {
        exit: function () { return (exiting = true); },
        get exiting() {
            return exiting;
        },
    };
}
exports.exitingFactory = exitingFactory;
//# sourceMappingURL=exiting.js.map