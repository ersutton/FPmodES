/**
 * function that takes an error function used for printing out any errors we get
 * additonally takes a tag string used for identifying where the error occured
 * and finally it takes the actual error you want to print
 *
 * @param {(value: string) => void} logFN
 * @param {(value: string) => void} errFN
 * @param {string} tag
 * @param {Error} err
 */
export declare function errLog(errFN: (value: string) => void, enableTimestamp: boolean, tag: string, err: Error): void;
export declare function pRejector(enableTimestamp: boolean, tag: string): (msg: string) => Promise<never>;
export interface IRejector {
    (msg: string): Promise<never>;
}
export declare const promiseRejectorWithTimeStamp: (tag: string) => IRejector;
export declare const promiseRejector: (tag: string) => IRejector;
export declare function thrower(enableTimestamp: boolean, tag: string): IThrower;
export interface IThrower {
    (msg: string): never;
}
export declare const throwWithTimeStamp: (tag: string) => IThrower;
export declare const throwError: (tag: string) => IThrower;
/**
 * delicious little helper utility for logging to the console
 *
 * @export
 * @param {(value: string) => void} logFN
 * @param {string} tag
 * @param {string} message
 */
export declare function tagLogger(logFN: (value: string) => void, enableTimestamp: boolean, tag: string, message: string | Object, enabled?: boolean): void;
