"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
/**
 * used in conjunction with errLog below to build an error string for logging
 *
 * @param {boolean} enableTimestamp
 * @param {string} tag
 * @returns {string}
 */
function tagBuilder(enableTimestamp, tag) {
    var timestamp = "[" + new Date(Date.now()).toLocaleTimeString() + "]";
    var tagString = "[" + tag + "]: ";
    return enableTimestamp ? "" + timestamp + tagString : tagString;
}
/**
 * function that takes an error function used for printing out any errors we get
 * additonally takes a tag string used for identifying where the error occured
 * and finally it takes the actual error you want to print
 *
 * @param {(value: string) => void} logFN
 * @param {(value: string) => void} errFN
 * @param {string} tag
 * @param {Error} err
 */
function errLog(errFN, enableTimestamp, tag, err) {
    var prefix = tagBuilder(enableTimestamp, tag);
    errFN(err.message ? "" + prefix + err.message : prefix + "has no err.message");
    errFN(err.stack ? err.stack : prefix + "has no err.stack");
}
exports.errLog = errLog;
function pRejector(enableTimestamp, tag) {
    var prefix = tagBuilder(enableTimestamp, tag);
    return function (msg) {
        return Promise.reject(new Error("" + prefix + msg));
    };
}
exports.pRejector = pRejector;
exports.promiseRejectorWithTimeStamp = __1.partial(pRejector, true);
exports.promiseRejector = __1.partial(pRejector, false);
function thrower(enableTimestamp, tag) {
    var prefix = tagBuilder(enableTimestamp, tag);
    return function (msg) {
        throw new Error("" + prefix + msg);
    };
}
exports.thrower = thrower;
exports.throwWithTimeStamp = __1.partial(thrower, true);
exports.throwError = __1.partial(thrower, false);
/**
 * delicious little helper utility for logging to the console
 *
 * @export
 * @param {(value: string) => void} logFN
 * @param {string} tag
 * @param {string} message
 */
function tagLogger(logFN, enableTimestamp, tag, message, enabled) {
    if (enabled === void 0) { enabled = true; }
    if (enabled !== true)
        return;
    var prefix = tagBuilder(enableTimestamp, tag);
    logFN("" + prefix + message);
}
exports.tagLogger = tagLogger;
//# sourceMappingURL=loggingHelpers.js.map