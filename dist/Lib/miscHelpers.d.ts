/**
 * file that contains useful functions that aren't strictly part of the functional programming library but are useful
 */
import { ObjectWithKeys } from '../index';
/**
 * function that deeply sets an objects property using a array as a path
 *
 * @export
 * @param {string[]} path
 * @param {*} obj
 * @param {*} value
 * @returns
 */
export declare function deepSetProp<T>(obj: T, path: string[], value: any): T;
/**
 * pure version of deepSetProp - well, we shallow copy the passed in object.
 * need to write a deepClone function....
 *
 * @export
 * @template T
 * @param {T} obj
 * @param {*} value
 * @returns {(...path: string[]) => T}
 */
export declare function deepSetPropPure<T>(obj: T, path: string[], value: any): T;
/**
 * deep getter function using a string[] path.
 *
 * @export
 * @param {string[]} path
 * @param {*} obj
 * @returns
 */
export declare function deepGetProp(obj: any, ...path: string[]): (...path: string[]) => any;
export declare function keyExistsOnObject(key: string, object: Object): boolean;
export declare function valueOfKeyOnObjectIsUndefined(key: string, object: Object): boolean;
export declare function keyExistsOnObjectAndIsntUndefined(key: string, object: Object): boolean;
/**
 * function that merges a single key from objectToMerge to mergedOntoObject.
 * however does not mutate the mergedOntoObject, it returns a clone using Object.assign.
 * note this is a shallow copy!
 * TODO: write test
 *
 * @export
 * @template ObjectType
 * @param {ObjectType} mergedOntoObject - the object you want to have values merged onto
 * @param {ObjectType} objectToMerge - the other object that you want to grab values from the merge onto the mergedOntoObject
 * @param {string} key - the key of the objectToMerge that you want to grab the value for and slot into the mergedOntoObject
 * @returns {ObjectType}
 */
export declare function objectKeyMerger<ObjectType>(mergedOntoObject: ObjectType, objectToMerge: ObjectType, key: string): ObjectType;
/**
 * oh boy. this function merges all keys and their values from the objectToMerge onto the mergedOntoObject.
 * we make a shallow copy of the passed in mergedOntoObject: WARN: this uses Object.assign and so wont deep clone objects!
 * TODO: write tests
 *
 * @export
 * @template ObjectType
 * @param {ObjectType} mergedOntoObject - the object you want to have values merged onto
 * @param {ObjectType} objectToMerge - the other object that you want to grab values from the merge onto the mergedOntoObject
 * @param {...string[]} keys - the key of the objectToMerge that you want to grab the value for and slot into the mergedOntoObject
 * @returns
 */
export declare function objectKeysMerger<ObjectType>(mergedOntoObject: ObjectType, objectToMerge: ObjectType, ...keys: string[]): ObjectType;
/**
 * function that takes a function and turns it into a Promise.
 * (required when I want to pass a mixture of functions into asyncPipe for example)
 * Note: This should not be used for a real asynchronous function with a callback that you
 * need to wrap in a promise. that you should do manually.
 *
 * @export
 * @template ReturnValue
 * @param {(...args: any[]) => ReturnValue} func
 * @returns {(...args: any[]) => Promise<ReturnValue>}
 */
export declare function promisify<ReturnValue>(func: (...args: any[]) => ReturnValue): (...args: any[]) => Promise<ReturnValue>;
export declare function stripKeyFromObject<ObjectType extends ObjectWithKeys>(key: string, object: ObjectType): Partial<ObjectType>;
export declare function stripKeyFromObjects<ObjectType extends ObjectWithKeys>(key: string, object: ObjectType[]): Partial<ObjectType>[];
export declare function getLastValueInArray<ObjectType>(values: ObjectType[]): ObjectType | undefined;
export declare function upperCaseFirstLetter(value: string): string;
/**
 * takes an enum and returns an array of all its values...
 *
 * @export
 * @template enumType
 * @param {enumType} e
 * @returns {number[]}
 */
export declare function enumArrayifier<enumType>(e: enumType): number[];
/**
 * no nice way of checking whether a string booleans, so wrote a super simple function to handle it
 *
 * @export
 * @param {string} bool
 * @returns {boolean}
 */
export declare function convertStringToBoolean(bool: string): boolean;
/**
 * Random Helper functions that have no better place to live.
 */
export interface ICancelableES6Promise<T> extends Promise<T> {
    cancel: () => void;
    additionalData: any;
}
/**
 * used when we need to dummy empty promises. borrowed from
 * http://rich.k3r.me/blog/2015/04/29/empty-promises-dos-and-donts-of-es6-promises/
 * @param val
 */
export declare function emptyPromise(): Promise<undefined>;
/**
 * Email Check. regex taken from https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript/1373724#1373724
 * by 'OregonTrail'. it is far from perfect, just simple validation. there is no perfect regex check for email.
 * still allow users that break this check to enter their email, they may have a weird email that is correct. instead, they should have a
 * 'test email' button to see if it works. use this email check just as a pop up warning that their email looks to be incorrect and they
 * should check it.
 *
 *
 * @param {String} email
 * @returns {boolean} valid
 */
export declare function emailCheck(email: string): boolean;
export declare type TTYpe = 'undefined' | 'object' | 'boolean' | 'number' | 'string' | 'symbol' | 'function';
export declare function isType(type: TTYpe, value: any): boolean;
export declare const isString: (value: any) => boolean;
/**
 * https://stackoverflow.com/questions/4878756/how-to-capitalize-first-letter-of-each-word-like-a-2-word-city
 * @param str
 */
export declare function toTitleCase(str: string): string;
export declare function isStringAndNotEmpty(str: string): boolean;
export declare function isAnUnEmptyArray(arr: any): boolean;
export declare function isAnUnEmptyArrayOfLength(arr: any, length: number): boolean;
export declare function isItNullOrUndefined(value: any): boolean;
export declare function isItNotNullOrNotUndefined(value: any): boolean;
/**
 * function that takes matches that look like this:
 * ["match", undefined, "another match"]
 * and returns the actual amount of things we found - which is 2
 *
 * @export
 * @param {RegExpMatchArray} match
 * @returns {number}
 */
export declare function RegexMatchCount(match: RegExpMatchArray | null): number;
export interface IStringMatches {
    foundAMatch: boolean;
    matches: RegExpMatchArray | null;
}
export declare function stringMatches(stringToMatch: string, match: RegExp): IStringMatches;
/**
 *
 * splits an array into an arry of arrays of length 'smallerArraySize'
 *
 * @export
 * @template T
 * @param {T[]} array
 * @param {number} smallerArraySize
 * @returns {T[][]}
 */
export declare function chunkArray<T>(array: T[], smallerArraySize: number): T[][];
/**
 * useful for combining strings into smaller substring arrays
 * chunkString("test",2) => ["te","st"]
 * chunkString("tests",2) => ["te","st","s"]
 * @export
 * @param {string} str
 * @param {number} length
 * @returns {string[]}
 */
export declare function chunkString(str: string, length: number): RegExpMatchArray | null;
/**
 * returns byte[] for a given string of hexidecimal characters
 *
 * @param {string} hex
 * @returns {number[]}
 */
export declare function hexToBytes(hex: string): number[];
/**
 * https://stackoverflow.com/questions/14028148/convert-integer-array-to-string-at-javascript
 *
 * @export
 * @param {number[]} byteArray
 * @returns {string}
 */
export declare function decodeUTF8ByteArrayToUTF16String(byteArray: number[]): string;
export interface IEither<T> {
    reason: T;
    err: Error[];
}
export interface IEitherWithResult<T, R> extends IEither<T> {
    result: R;
}
