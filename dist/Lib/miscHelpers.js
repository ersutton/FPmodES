"use strict";
/**
 * file that contains useful functions that aren't strictly part of the functional programming library but are useful
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var index_1 = require("../FP/index");
/**
 * function that deeply sets an objects property using a array as a path
 *
 * @export
 * @param {string[]} path
 * @param {*} obj
 * @param {*} value
 * @returns
 */
function deepSetProp(obj, path, value) {
    var copiedPath = path.slice();
    return (function objectSet(object) {
        var key = copiedPath.shift();
        if (copiedPath.length === 0) {
            if (key === undefined)
                return obj;
            index_1.setProp(key, object, value);
            return obj;
        }
        return key === undefined ? object : objectSet(index_1.getProp(key, object));
    })(obj);
}
exports.deepSetProp = deepSetProp;
/**
 * pure version of deepSetProp - well, we shallow copy the passed in object.
 * need to write a deepClone function....
 *
 * @export
 * @template T
 * @param {T} obj
 * @param {*} value
 * @returns {(...path: string[]) => T}
 */
function deepSetPropPure(obj, path, value) {
    return deepSetProp(__assign({}, obj), path, value);
}
exports.deepSetPropPure = deepSetPropPure;
/**
 * deep getter function using a string[] path.
 *
 * @export
 * @param {string[]} path
 * @param {*} obj
 * @returns
 */
function deepGetProp(obj) {
    var path = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        path[_i - 1] = arguments[_i];
    }
    var copiedPath = path.slice();
    return (function valueGetter(object) {
        var key = copiedPath.shift();
        if (key === undefined)
            return undefined;
        if (copiedPath.length === 0) {
            return index_1.getProp(key, object);
        }
        return valueGetter(index_1.getProp(key, object));
    })(obj);
}
exports.deepGetProp = deepGetProp;
function keyExistsOnObject(key, object) {
    return (Object.keys(object).find(function (value) { return value === key; }) !== undefined);
}
exports.keyExistsOnObject = keyExistsOnObject;
function valueOfKeyOnObjectIsUndefined(key, object) {
    return index_1.getProp(key, object) === undefined;
}
exports.valueOfKeyOnObjectIsUndefined = valueOfKeyOnObjectIsUndefined;
function keyExistsOnObjectAndIsntUndefined(key, object) {
    return (keyExistsOnObject(key, object) &&
        !valueOfKeyOnObjectIsUndefined(key, object));
}
exports.keyExistsOnObjectAndIsntUndefined = keyExistsOnObjectAndIsntUndefined;
/**
 * function that merges a single key from objectToMerge to mergedOntoObject.
 * however does not mutate the mergedOntoObject, it returns a clone using Object.assign.
 * note this is a shallow copy!
 * TODO: write test
 *
 * @export
 * @template ObjectType
 * @param {ObjectType} mergedOntoObject - the object you want to have values merged onto
 * @param {ObjectType} objectToMerge - the other object that you want to grab values from the merge onto the mergedOntoObject
 * @param {string} key - the key of the objectToMerge that you want to grab the value for and slot into the mergedOntoObject
 * @returns {ObjectType}
 */
function objectKeyMerger(mergedOntoObject, objectToMerge, key) {
    var finalObject = Object.assign({}, mergedOntoObject);
    keyExistsOnObjectAndIsntUndefined(key, objectToMerge)
        ? index_1.setProp(key, finalObject, index_1.getProp(key, objectToMerge))
        : undefined;
    return finalObject;
}
exports.objectKeyMerger = objectKeyMerger;
/**
 * oh boy. this function merges all keys and their values from the objectToMerge onto the mergedOntoObject.
 * we make a shallow copy of the passed in mergedOntoObject: WARN: this uses Object.assign and so wont deep clone objects!
 * TODO: write tests
 *
 * @export
 * @template ObjectType
 * @param {ObjectType} mergedOntoObject - the object you want to have values merged onto
 * @param {ObjectType} objectToMerge - the other object that you want to grab values from the merge onto the mergedOntoObject
 * @param {...string[]} keys - the key of the objectToMerge that you want to grab the value for and slot into the mergedOntoObject
 * @returns
 */
function objectKeysMerger(mergedOntoObject, objectToMerge) {
    var keys = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        keys[_i - 2] = arguments[_i];
    }
    var objectCopy = Object.assign({}, mergedOntoObject);
    function mergeValues(object, mergeObject, copiedKeys) {
        if (copiedKeys.length > 0) {
            // it cannot be undefined as we have checked length
            var key = copiedKeys.pop();
            object = objectKeyMerger(object, mergeObject, key);
            return mergeValues(object, mergeObject, copiedKeys);
        }
        return object;
    }
    return mergeValues(objectCopy, objectToMerge, keys.slice());
}
exports.objectKeysMerger = objectKeysMerger;
/**
 * function that takes a function and turns it into a Promise.
 * (required when I want to pass a mixture of functions into asyncPipe for example)
 * Note: This should not be used for a real asynchronous function with a callback that you
 * need to wrap in a promise. that you should do manually.
 *
 * @export
 * @template ReturnValue
 * @param {(...args: any[]) => ReturnValue} func
 * @returns {(...args: any[]) => Promise<ReturnValue>}
 */
function promisify(func) {
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return Promise.resolve(func.apply(void 0, args));
    };
}
exports.promisify = promisify;
function stripKeyFromObject(key, object) {
    var cloneOfObject = Object.assign({}, object);
    if (keyExistsOnObject(key, cloneOfObject)) {
        delete cloneOfObject[key];
    }
    return cloneOfObject;
}
exports.stripKeyFromObject = stripKeyFromObject;
function stripKeyFromObjects(key, object) {
    return object.map(function (obj) { return stripKeyFromObject(key, obj); });
}
exports.stripKeyFromObjects = stripKeyFromObjects;
function getLastValueInArray(values) {
    if (!Array.isArray(values))
        return undefined;
    return values.slice(-1).pop();
}
exports.getLastValueInArray = getLastValueInArray;
function upperCaseFirstLetter(value) {
    if (exports.isString(value) && value.length > 0) {
        var capitalStart = value[0].toUpperCase();
        return "" + capitalStart + value.substr(1);
    }
    return value;
}
exports.upperCaseFirstLetter = upperCaseFirstLetter;
/**
 * takes an enum and returns an array of all its values...
 *
 * @export
 * @template enumType
 * @param {enumType} e
 * @returns {number[]}
 */
function enumArrayifier(e) {
    return Object.keys(e)
        .map(function (key) {
        return Number(key);
    })
        .filter(function (stateType) { return !isNaN(stateType); });
}
exports.enumArrayifier = enumArrayifier;
/**
 * no nice way of checking whether a string booleans, so wrote a super simple function to handle it
 *
 * @export
 * @param {string} bool
 * @returns {boolean}
 */
function convertStringToBoolean(bool) {
    if (typeof bool !== 'string')
        return false; // needs to be a string
    return bool.toLowerCase() === 'true'; // needs to be exactly true
}
exports.convertStringToBoolean = convertStringToBoolean;
/**
 * used when we need to dummy empty promises. borrowed from
 * http://rich.k3r.me/blog/2015/04/29/empty-promises-dos-and-donts-of-es6-promises/
 * @param val
 */
function emptyPromise() {
    return new Promise(function (resolve) {
        resolve(undefined);
    });
}
exports.emptyPromise = emptyPromise;
/**
 * Email Check. regex taken from https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript/1373724#1373724
 * by 'OregonTrail'. it is far from perfect, just simple validation. there is no perfect regex check for email.
 * still allow users that break this check to enter their email, they may have a weird email that is correct. instead, they should have a
 * 'test email' button to see if it works. use this email check just as a pop up warning that their email looks to be incorrect and they
 * should check it.
 *
 *
 * @param {String} email
 * @returns {boolean} valid
 */
function emailCheck(email) {
    var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
    // if (typeof email !== 'string') return false;
    // if (email.length < 5) return false;
    // // If '@' is missing, or at index 0, return false
    // if (email.indexOf('@') < 1) return false;
    // // If '.' is missing, or at index 0, return false
    // if (email.indexOf('.') < 1) return false;
    // return true;
}
exports.emailCheck = emailCheck;
function isType(type, value) {
    return typeof value === type;
}
exports.isType = isType;
exports.isString = index_1.partial(isType, 'string');
/**
 * https://stackoverflow.com/questions/4878756/how-to-capitalize-first-letter-of-each-word-like-a-2-word-city
 * @param str
 */
function toTitleCase(str) {
    if (!exports.isString(str) || str.length === 0)
        return str;
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}
exports.toTitleCase = toTitleCase;
function isStringAndNotEmpty(str) {
    return exports.isString(str) && str.length > 0;
}
exports.isStringAndNotEmpty = isStringAndNotEmpty;
function isAnUnEmptyArray(arr) {
    return Array.isArray(arr) && arr.length > 0;
}
exports.isAnUnEmptyArray = isAnUnEmptyArray;
function isAnUnEmptyArrayOfLength(arr, length) {
    return Array.isArray(arr) && arr.length === length;
}
exports.isAnUnEmptyArrayOfLength = isAnUnEmptyArrayOfLength;
function isItNullOrUndefined(value) {
    if (value === null)
        return true;
    if (value === undefined)
        return true;
    return false;
}
exports.isItNullOrUndefined = isItNullOrUndefined;
function isItNotNullOrNotUndefined(value) {
    return !isItNullOrUndefined(value);
}
exports.isItNotNullOrNotUndefined = isItNotNullOrNotUndefined;
/**
 * function that takes matches that look like this:
 * ["match", undefined, "another match"]
 * and returns the actual amount of things we found - which is 2
 *
 * @export
 * @param {RegExpMatchArray} match
 * @returns {number}
 */
function RegexMatchCount(match) {
    if (match === undefined || match === null)
        return -1;
    // undefined's or nulls are falsy, and will get filtered out..
    return match.filter(function (val) { return val; }).length;
}
exports.RegexMatchCount = RegexMatchCount;
function stringMatches(stringToMatch, match) {
    var matches = stringToMatch.match(match);
    var foundAMatch = isAnUnEmptyArray(matches);
    return {
        foundAMatch: foundAMatch,
        matches: matches,
    };
}
exports.stringMatches = stringMatches;
/**
 *
 * splits an array into an arry of arrays of length 'smallerArraySize'
 *
 * @export
 * @template T
 * @param {T[]} array
 * @param {number} smallerArraySize
 * @returns {T[][]}
 */
function chunkArray(array, smallerArraySize) {
    return array
        .map(function (value, index) {
        return index % smallerArraySize === 0
            ? array.slice(index, index + smallerArraySize)
            : undefined;
    })
        .filter(function (value) { return !util_1.isUndefined(value); });
}
exports.chunkArray = chunkArray;
/**
 * useful for combining strings into smaller substring arrays
 * chunkString("test",2) => ["te","st"]
 * chunkString("tests",2) => ["te","st","s"]
 * @export
 * @param {string} str
 * @param {number} length
 * @returns {string[]}
 */
function chunkString(str, length) {
    return str.match(new RegExp('.{1,' + length + '}', 'g'));
}
exports.chunkString = chunkString;
/**
 * returns byte[] for a given string of hexidecimal characters
 *
 * @param {string} hex
 * @returns {number[]}
 */
function hexToBytes(hex) {
    var chunks = chunkString(hex, 2);
    if (util_1.isNull(chunks))
        return [];
    return chunks.map(function (byte) {
        return parseInt(byte, 16);
    });
}
exports.hexToBytes = hexToBytes;
/**
 * https://stackoverflow.com/questions/14028148/convert-integer-array-to-string-at-javascript
 *
 * @export
 * @param {number[]} byteArray
 * @returns {string}
 */
function decodeUTF8ByteArrayToUTF16String(byteArray) {
    return decodeURIComponent(byteArray
        .map(function (value) {
        return "%" + ('0' + value.toString(16)).slice(-2); // the slice -2 is to make it only get the last two characters...so 042 becomes 42 which then becomes %42
    })
        .join(''));
}
exports.decodeUTF8ByteArrayToUTF16String = decodeUTF8ByteArrayToUTF16String;
//# sourceMappingURL=miscHelpers.js.map