"use strict";
// // lets play with functional mixins... lets see if we can compose functions together
// import * as stampit from 'stampit';
// export interface IQuack {
//   quackSound: string;
//   quack(): string;
// }
// export const quackerStamp: () => IQuack = stampit({
//   props: {
//     quackSound: 'Quack',
//   },
//   methods: {
//     quack() {
//       return this.quackSound;
//     },
//   },
// });
// export interface IFlyer {
//   _flying: string;
//   isFlying(): boolean;
//   fly(): IFlyer;
//   land(): IFlyer;
// }
// export const flyerStamp: () => IFlyer = stampit({
//   props: {
//     _flying: false,
//   },
//   methods: {
//     fly() {
//       this._flying = true;
//       return this;
//     },
//     land() {
//       this._flying = false;
//       return this;
//     },
//     isFlying() {
//       return this._flying;
//     },
//   },
// });
// export const duckStamp: () => IQuack & IFlyer = stampit(
//   quackerStamp,
//   flyerStamp
// );
//# sourceMappingURL=modIt.js.map