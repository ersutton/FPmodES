export * from './FP';
export * from './Lib';
/**
 *
 *
 * @export
 * @interface ObjectWithKeys
 * @extends {Object}
 * @deprecated use Record instead
 */
export interface ObjectWithKeys extends Object {
    [key: string]: any;
}
/** from https://levelup.gitconnected.com/advanced-typescript-types-with-examples-1d144e4eda9e */
export declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export declare type Head<T extends any[]> = T extends [any, ...any[]] ? T[0] : never;
export declare type Tail<T extends any[]> = ((...t: T) => any) extends ((_: any, ...tail: infer TT) => any) ? TT : [];
export declare type HasTail<T extends any[]> = T extends ([] | [any]) ? false : true;
export declare type FunctionInfer<F> = F extends (...args: infer A) => infer R ? [A, R] : never;
export declare type Last<T extends any[]> = {
    0: Last<Tail<T>>;
    1: Head<T>;
}[HasTail<T> extends true ? 0 : 1];
export declare type Length<T extends any[]> = T['length'];
export declare type Prepend<E, T extends any[]> = ((head: E, ...args: T) => any) extends ((...args: infer U) => any) ? U : T;
export declare type Drop<N extends number, T extends any[], I extends any[] = []> = {
    0: Drop<N, Tail<T>, Prepend<any, I>>;
    1: T;
}[Length<I> extends N ? 1 : 0];
export declare type Cast<X, Y> = X extends Y ? X : Y;
export declare type Pos<I extends any[]> = Length<I>;
export declare type Next<I extends any[]> = Prepend<any, I>;
export declare type Prev<I extends any[]> = Tail<I>;
export declare type Iterator<Index extends number = 0, From extends any[] = [], I extends any[] = []> = {
    0: Iterator<Index, Next<From>, Next<I>>;
    1: From;
}[Pos<I> extends Index ? 1 : 0];
export declare type Reverse<T extends any[], R extends any[] = [], I extends any[] = []> = {
    0: Reverse<T, Prepend<T[Pos<I>], R>, Next<I>>;
    1: R;
}[Pos<I> extends Length<T> ? 1 : 0];
