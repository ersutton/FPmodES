"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./FP"));
__export(require("./Lib"));
// export type Concat<T1 extends any[], T2 extends any[]> = Reverse<
//   Cast<Reverse<T1>, any[]>,
//   T2
// >;
// export type Append<E, T extends any[]> = Concat<T, [E]>;
// export type __ = typeof R.__;
// export type GapOf<
//   T1 extends any[],
//   T2 extends any[],
//   TN extends any[],
//   I extends any[]
// > = T1[Pos<I>] extends __ ? Append<T2[Pos<I>], TN> : TN;
// export type GapsOf<
//   T1 extends any[],
//   T2 extends any[],
//   TN extends any[] = [],
//   I extends any[] = []
// > = {
//   0: GapsOf<T1, T2, Cast<GapOf<T1, T2, TN, I>, any[]>, Next<I>>;
//   1: Concat<TN, Cast<Drop<Pos<I>, T2>, any[]>>;
// }[Pos<I> extends Length<T1> ? 1 : 0];
// export type PartialGaps<T extends any[]> = { [K in keyof T]?: T[K] | __ };
// export type CleanedGaps<T extends any[]> = {
//   [K in keyof T]: NonNullable<T[K]>
// };
// export type Gaps<T extends any[]> = CleanedGaps<PartialGaps<T>>;
// export type CurryV6<P extends any[], R> = <T extends any[]>(
//   ...args: Cast<T, Gaps<P>>
// ) => GapsOf<T, P> extends [any, ...any[]]
//   ? CurryV6<Cast<GapsOf<T, P>, any[]>, R>
//   : R;
// export type Curry<F extends (...args: any) => any> = <T extends any[]>(
//   ...args: Cast<Cast<T, Gaps<Parameters<F>>>, any[]>
// ) => GapsOf<T, Parameters<F>> extends [any, ...any[]]
//   ? Curry<(...args: Cast<GapsOf<T, Parameters<F>>, any[]>) => ReturnType<F>>
//   : ReturnType<F>;
//# sourceMappingURL=index.js.map