import { reverseArgs } from './functional';

/**
 * async functions untested.
 */

/**
 * a promise based pipe. instead of writing things like this:
 * something.then(somethingelse).then(somethingElseAgain)
 *
 * you can write it like this: asyncPiped(something,somethingElse,somethingElseAgain);
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(result: any) => Promise<any>}
 */
export function asyncPipe<ReturnType, ReturnMethodArgType>(
  ...fns: ((value?: any) => Promise<any>)[]
): (result: ReturnMethodArgType) => Promise<ReturnType | undefined> {
  return function asyncPiped(
    result: ReturnMethodArgType
  ): Promise<ReturnType | undefined> {
    // need to take a copy of the functions else the slice inside will ruin it and it will only run once
    const copiedFunctions: ((value?: any) => Promise<any>)[] = fns.slice();
    function asyncPiper(result: any): Promise<any> {
      if (copiedFunctions.length > 0) {
        return (copiedFunctions.shift() as ((value?: any) => Promise<any>))(
          result
        ).then(asyncPiper);
      }
      return result as Promise<ReturnType>;
    }
    return asyncPiper(result);
  };
}

/**
 * a promise based compose. instead of writing things like this:
 * something.then(somethingelse).then(somethingElseAgain)
 *
 * you can write it like this: asyncPiped(somethingElseAgain,somethingelse,something);
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(result: any) => Promise<any>}
 */
export const asyncCompose: (result: any) => Promise<any> = reverseArgs(
  asyncPipe
);

/**
 * excute an array of promise factories in sequential order
 * stolen from here: https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
 *
 * @export
 * @param {(() => Promise<any>)[]} promiseFactories
 * @returns {Promise<any>}
 */
export function excecuteSequentially(
  promiseFactories: (() => Promise<any>)[]
): Promise<any> {
  let result: Promise<any> = Promise.resolve();
  promiseFactories.forEach((promiseFactory: () => Promise<any>) => {
    result = result.then(promiseFactory);
  });
  return result;
}
