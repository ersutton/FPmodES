import { ObjectWithKeys } from '../index';

/**
 * Library that borrows heavily from https://github.com/getify/Functional-Light-JS
 *
 * Reason for this library is for my own learning outcomes - get to play in Functional Programming land in the hope to use some of these techniques in real projects
 *
 * Typescript Interface Guide
 * I have added optional interface parameters so that if you want, you can use this library with typescript and have fairly indepth typechecking
 * however, by adding these interfaces we are telling the TS compiler if a number is passed in as a parameter, a number is expected as the return type. we will get compiler warnings if we misuse these functions. great stuff
 *
 * RT = ReturnType - This is the Return Values expected type
 * AT = ArgumentType - this is an arguments type
 * T = Generic Type - may be something that is both passed in and returned
 */

/**
 * First function in https://github.com/getify/Functional-Light-JS/blob/master/ch3.md
 * used to ensure that a function can only ever receive a single argument, an arity of 1.
 *
 * Consider:
 * ["1","2","3"].map( parseInt );
 * would result in:
 * [1,NaN,NaN]
 *
 * this is due to parseInts signature is:
 * parseInt(str,radix) radix is its base (like base 2 or base 10), see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
 *
 * whereas with this simple unary function we can do this:
 * ["1","2","3"].map( unary( parseInt ) );
 * [1,2,3]
 *
 * @export
 * @template RT
 * @template AT
 * @param {Function} fn
 * @returns {(arg: AT) => RT}
 */
export function unary<RT, AT>(fn: Function): (arg: AT) => RT {
  return function onlyOneArg(arg: AT) {
    return fn(arg);
  };
}

/**
 * A seemingly pointless function from https://github.com/getify/Functional-Light-JS/blob/master/ch3.md
 * all it does is return the value passed into it. This is useful for filtering arrays with empty values:
 *
 * var words = "   Now is the time for all...  ".split( /\s|\b/ );
 * words;
 * ["","Now","is","the","time","for","all","...",""]
 *
 * words.filter( identity );
 * ["Now","is","the","time","for","all","..."]
 *
 * @export
 * @template T
 * @param {T} value
 * @returns {T}
 */
export function identity<T>(value: T): T {
  return value;
}

/**
 * Certain APIs don't let you pass a value directly into a method,
 * but require you to pass in a function, even if that function literally just returns the value.
 * One such API is the then(..) method on JS Promises:
 *
 * With this tidy little FP utility, we can solve our then(..) annoyance properly:
 * p1.then( foo ).then( constant( p2 ) ).then( bar );
 *
 * @export
 * @template T
 * @param {T} v
 * @returns {() => T}
 */
export function constant<T>(v: T): () => T {
  return function value() {
    return v;
  };
}
/**
 * function that is used to bend function signatures to our will.
 * we can pass in a function we want to change the parameters for and it will return a function that will play nice.
 * if we have a function that looks like this:
 * myFunction(x,y)
 * and we want it to behave like this:
 * myFunction([x,y])
 * we can use this function to convert it
 *
 * @export
 * @param {Function} fn
 * @returns {*}
 */
const spreadArgs: (
  fn: Function
) => (argsArr: any[]) => any = function spreadArgs(fn: Function): any {
  return function spreadFn(argsArr: any[]) {
    return fn(...argsArr);
  };
};
/**
 * see spreadArgs above
 *
 * @param {Function} fn
 * @returns {*}
 */
export const apply = spreadArgs;

/**
 * function that is used to bend function signatures to our will.
 * we can pass in a function we want to change the parameters for and it will return a function that will play nice.
 * if we have a function that looks like this:
 * myFunction([x,y])
 * and we want it to behave like this:
 * myFunction(x,y)
 * we can use this function to convert it
 *
 * @export
 * @param {Function} fn
 * @returns {*}
 */
export const gatherArgs: (
  fn: Function
) => (...argsArr: any[]) => any = function gatherArgs(fn: Function): any {
  return function gatheredFn(...argsArr: any[]) {
    return fn(argsArr);
  };
};
/**
 * see gatherArgs above
 *
 * @param {Function} fn
 * @returns {*}
 */
export const unapply = gatherArgs;

/**
 * Function that is used to partially apply function parameters from left to right.
 * So lets say we have a function (x,y,z) => any, but we know ahead of time what x and y are,
 * and we want instead to have a function that is used at the call site that is (z) => any. going from an arity of 3 to 1. shiny.0
 * this function you could call like this:
 * partial(fn, x,y) => reducedArityFn(z) => any
 *
 * @export
 * @param {Function} fn
 * @param {...any[]} presetArgs
 * @returns {(...laterArgs: any[]) => any}
 */
export function partial(
  fn: Function,
  ...presetArgs: any[]
): (...laterArgs: any[]) => any {
  return function partiallyApplied(...laterArgs: any[]) {
    return fn(...presetArgs, ...laterArgs);
  };
}
/**
 * function that allows you to pass in a function and get a new function back with the args reversed. useful if we want to use partial above and set the final parameters.
 *
 * @export
 * @param {Function} fn
 * @returns {(...args: any[]) => any}
 */
export function reverseArgs(fn: Function): (...args: any[]) => any {
  return function argsReversed(...args: any[]) {
    return fn(...args.reverse());
  };
}
/**
 * function that allows us to partially apply function parameters from right to left.
 * to achieve this we first have to reverse the args in the passed in function. we then partially apply this
 * newly reverse args'd function with the passed in partial parameters array. we then reverse taht back.
 * this returns a function that behaves like the originally passed in function, only with reduced arity.
 *
 * @export
 * @param {Function} fn
 * @param {...any[]} presetArgs
 * @returns {(...args: any[]) => any}
 */
export function partialRight(
  fn: Function,
  ...presetArgs: any[]
): (...args: any[]) => any {
  return reverseArgs(partial(reverseArgs(fn), ...presetArgs));
}

export interface ICurry {
  (arg: any): ICurry | any;
}
/**
 * currys a function. returns a function that takes the next argument, unless it is the last
 * argument, in which case it calls the original function with all the parameters.
 *
 * @export
 * @param {Function} fn
 * @param {number} [arity=fn.length]
 * @returns {ICurry}
 */
export function curry<P extends any[], R>(
  fn: Function,
  arity: number = fn.length
): ICurry {
  // when first called this IIFE returns the curried version of the function, intiating the args array to be an empty array.
  return (function nextCurried(prevArgs: any[]) {
    /*  
      this is the function that is affectively returned when 'curry' is first called, with prevArgs set to []. 
      every successive call will build up this array until the arity is met
    */
    return function curried(nextArg: any) {
      var args: any[] = [...prevArgs, nextArg];

      if (args.length >= arity) {
        return fn(...args);
      } else {
        return nextCurried(args);
      }
    };
  })([]);
}

/**
 * this allows the composition of functions with the order of execution from left to right
 *
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(val: any) => any}
 */
export function pipe<T, U>(
  ...fns: Function[]
): (parameterToPassToFirstFunction?: U) => T {
  return function composed(parameterToPassToFirstFunction?: U): T {
    return fns.reduce(
      (result: any, fn: Function, index: number, array: Function[]): T => {
        return fn(result) as T;
      },
      (parameterToPassToFirstFunction as unknown) as T
    );
  };
}

/**
 * this allows the composition of functions with the order of execution from right to left
 *
 *
 * @export
 * @param {...Function[]} fns
 * @returns {(val: any) => any}
 */
export const compose = reverseArgs(pipe);
/**
 * gets the value of the object at the specified key
 *
 * @export
 * @param {string} key
 * @param {*} object
 * @returns {*}
 */
export function getProp<ObjectType extends ObjectWithKeys, ReturnType>(
  key: string,
  object: ObjectType
): ReturnType {
  return object[key];
}
/**
 * sets the passed in object's value at the key specified. returns the object. Pure Version
 * for impure version use setProp
 * @param name
 * @param object
 * @param value
 */
export function setPropPure<ObjectType>(
  key: string,
  object: ObjectType,
  value: any
): ObjectType {
  if (object !== undefined) {
    const newObj = Object.assign({}, object);
    return setProp(key, newObj, value);
  }
  return object;
}

/**
 * impure setProp - if you want a pure version use setPropPure
 *
 * @export
 * @param {string} key
 * @param {*} object
 * @param {*} value
 * @returns {*}
 */
export function setProp(key: string, object: any, value: any) {
  if (object !== undefined) object[key] = value;
  return object;
}
/**
 * creates a new object with the named key and a value assigned to it.
 *
 * @export
 * @param {string} key
 * @param {*} value
 * @returns
 */
export function makeObjProp(key: string, value: any) {
  return setProp(key, {}, value);
}
