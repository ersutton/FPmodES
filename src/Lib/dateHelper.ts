import { partial } from '..';

/**
 * really small timehelper, would rather use this than the huge time libraries like moment
 */

export interface ITimeHelper {
  secondsInMilliSeconds: (seconds: number) => number;
  minutesInMilliSeconds: (minutes: number) => number;
  hoursInMilliSeconds: (hours: number) => number;
  daysInMilliSeconds: (days: number) => number;
  monthsInMilliSeconds: (months: number) => number;
}
export interface ITimeHelperSingleton {
  (): ITimeHelper;
}

export const TimeHelper: ITimeHelperSingleton = (function() {
  let timeHelper: ITimeHelper;

  function createInstance(): ITimeHelper {
    const millisecondsInASecond: number = 1000;
    const millisecondsInAMinute: number = multiply(millisecondsInASecond, 60);
    const milliSecondsInAnHour: number = multiply(millisecondsInAMinute, 60);
    const milliSecondsInADay: number = multiply(milliSecondsInAnHour, 24);
    const milliSecondsInAMonth: number = multiply(milliSecondsInADay, 30);

    function multiply(number1: number, number2: number): number {
      return number1 * number2;
    }

    return {
      secondsInMilliSeconds: partial(multiply, millisecondsInASecond),
      minutesInMilliSeconds: partial(multiply, millisecondsInAMinute),
      hoursInMilliSeconds: partial(multiply, milliSecondsInAnHour),
      daysInMilliSeconds: partial(multiply, milliSecondsInADay),
      monthsInMilliSeconds: partial(multiply, milliSecondsInAMonth),
    };
  }

  return function getInstance(): ITimeHelper {
    if (timeHelper === undefined) {
      timeHelper = createInstance();
    }
    return timeHelper;
  };
})();
