// fairly overkill, just needed a simple object for holding exit state

export interface IExitingObj {
  exit(): void;
  exiting: boolean;
}
/**
 * simple object for storing exiting state. as it is an object that
 * can be passed into several places, all can monitor the exiting state
 * because it is part of an object. mutating it in one place mutates it everywhere (pass by reference)
 *
 * @returns {IExitingObj}
 */
export function exitingFactory(): IExitingObj {
  var exiting: boolean = false;
  return {
    exit: () => (exiting = true),
    get exiting() {
      return exiting;
    },
  };
}
