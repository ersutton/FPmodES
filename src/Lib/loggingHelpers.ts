import { partial } from '..';

/**
 * used in conjunction with errLog below to build an error string for logging
 *
 * @param {boolean} enableTimestamp
 * @param {string} tag
 * @returns {string}
 */
function tagBuilder(enableTimestamp: boolean, tag: string): string {
  const timestamp: string = `[${new Date(Date.now()).toLocaleTimeString()}]`;
  const tagString: string = `[${tag}]: `;
  return enableTimestamp ? `${timestamp}${tagString}` : tagString;
}
/**
 * function that takes an error function used for printing out any errors we get
 * additonally takes a tag string used for identifying where the error occured
 * and finally it takes the actual error you want to print
 *
 * @param {(value: string) => void} logFN
 * @param {(value: string) => void} errFN
 * @param {string} tag
 * @param {Error} err
 */
export function errLog(
  errFN: (value: string) => void,
  enableTimestamp: boolean,
  tag: string,
  err: Error
): void {
  const prefix = tagBuilder(enableTimestamp, tag);
  errFN(
    err.message ? `${prefix}${err.message}` : `${prefix}has no err.message`
  );
  errFN(err.stack ? err.stack : `${prefix}has no err.stack`);
}

export function pRejector(
  enableTimestamp: boolean,
  tag: string
): (msg: string) => Promise<never> {
  const prefix = tagBuilder(enableTimestamp, tag);
  return (msg: string) => {
    return Promise.reject(new Error(`${prefix}${msg}`));
  };
}
export interface IRejector {
  (msg: string): Promise<never>;
}

export const promiseRejectorWithTimeStamp: (tag: string) => IRejector = partial(
  pRejector,
  true
);
export const promiseRejector: (tag: string) => IRejector = partial(
  pRejector,
  false
);

export function thrower(enableTimestamp: boolean, tag: string): IThrower {
  const prefix = tagBuilder(enableTimestamp, tag);
  return (msg: string) => {
    throw new Error(`${prefix}${msg}`);
  };
}
export interface IThrower {
  (msg: string): never;
}

export const throwWithTimeStamp: (tag: string) => IThrower = partial(
  thrower,
  true
);
export const throwError: (tag: string) => IThrower = partial(thrower, false);

/**
 * delicious little helper utility for logging to the console
 *
 * @export
 * @param {(value: string) => void} logFN
 * @param {string} tag
 * @param {string} message
 */
export function tagLogger(
  logFN: (value: string) => void,
  enableTimestamp: boolean,
  tag: string,
  message: string | Object,
  enabled = true
) {
  if (enabled !== true) return;
  const prefix = tagBuilder(enableTimestamp, tag);
  logFN(`${prefix}${message}`);
}
