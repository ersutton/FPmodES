/**
 * file that contains useful functions that aren't strictly part of the functional programming library but are useful
 */

import { ObjectWithKeys } from '../index';
import { isUndefined, isNull } from 'util';
import { setProp, getProp, partial } from '../FP/index';

/**
 * function that deeply sets an objects property using a array as a path
 *
 * @export
 * @param {string[]} path
 * @param {*} obj
 * @param {*} value
 * @returns
 */
export function deepSetProp<T>(obj: T, path: string[], value: any): T {
  const copiedPath: string[] = [...path];
  return (function objectSet(object: T): T {
    const key: string | undefined = copiedPath.shift();
    if (copiedPath.length === 0) {
      if (key === undefined) return obj;
      setProp(key, object, value);
      return obj;
    }
    return key === undefined ? object : objectSet(getProp(key, object));
  })(obj);
}

/**
 * pure version of deepSetProp - well, we shallow copy the passed in object.
 * need to write a deepClone function....
 *
 * @export
 * @template T
 * @param {T} obj
 * @param {*} value
 * @returns {(...path: string[]) => T}
 */
export function deepSetPropPure<T>(obj: T, path: string[], value: any): T {
  return deepSetProp({ ...obj }, path, value);
}
/**
 * deep getter function using a string[] path.
 *
 * @export
 * @param {string[]} path
 * @param {*} obj
 * @returns
 */
export function deepGetProp(
  obj: any,
  ...path: string[]
): (...path: string[]) => any {
  const copiedPath = [...path];
  return (function valueGetter(object: any): any {
    const key: string | undefined = copiedPath.shift();
    if (key === undefined) return undefined;
    if (copiedPath.length === 0) {
      return getProp(key, object);
    }
    return valueGetter(getProp(key, object));
  })(obj);
}

export function keyExistsOnObject(key: string, object: Object): boolean {
  return (
    Object.keys(object).find((value: string) => value === key) !== undefined
  );
}

export function valueOfKeyOnObjectIsUndefined(
  key: string,
  object: Object
): boolean {
  return getProp(key, object) === undefined;
}

export function keyExistsOnObjectAndIsntUndefined(
  key: string,
  object: Object
): boolean {
  return (
    keyExistsOnObject(key, object) &&
    !valueOfKeyOnObjectIsUndefined(key, object)
  );
}

/**
 * function that merges a single key from objectToMerge to mergedOntoObject.
 * however does not mutate the mergedOntoObject, it returns a clone using Object.assign.
 * note this is a shallow copy!
 * TODO: write test
 *
 * @export
 * @template ObjectType
 * @param {ObjectType} mergedOntoObject - the object you want to have values merged onto
 * @param {ObjectType} objectToMerge - the other object that you want to grab values from the merge onto the mergedOntoObject
 * @param {string} key - the key of the objectToMerge that you want to grab the value for and slot into the mergedOntoObject
 * @returns {ObjectType}
 */
export function objectKeyMerger<ObjectType>(
  mergedOntoObject: ObjectType,
  objectToMerge: ObjectType,
  key: string
): ObjectType {
  const finalObject = Object.assign({}, mergedOntoObject);
  keyExistsOnObjectAndIsntUndefined(key, objectToMerge)
    ? setProp(key, finalObject, getProp(key, objectToMerge))
    : undefined;
  return finalObject as ObjectType;
}

/**
 * oh boy. this function merges all keys and their values from the objectToMerge onto the mergedOntoObject.
 * we make a shallow copy of the passed in mergedOntoObject: WARN: this uses Object.assign and so wont deep clone objects!
 * TODO: write tests
 *
 * @export
 * @template ObjectType
 * @param {ObjectType} mergedOntoObject - the object you want to have values merged onto
 * @param {ObjectType} objectToMerge - the other object that you want to grab values from the merge onto the mergedOntoObject
 * @param {...string[]} keys - the key of the objectToMerge that you want to grab the value for and slot into the mergedOntoObject
 * @returns
 */
export function objectKeysMerger<ObjectType>(
  mergedOntoObject: ObjectType,
  objectToMerge: ObjectType,
  ...keys: string[]
): ObjectType {
  const objectCopy: ObjectType = Object.assign({}, mergedOntoObject);
  function mergeValues(
    object: ObjectType,
    mergeObject: ObjectType,
    copiedKeys: string[]
  ): ObjectType {
    if (copiedKeys.length > 0) {
      // it cannot be undefined as we have checked length
      const key: string = copiedKeys.pop() as string;

      object = objectKeyMerger(object, mergeObject, key);
      return mergeValues(object, mergeObject, copiedKeys);
    }
    return object;
  }
  return mergeValues(objectCopy, objectToMerge, keys.slice());
}

/**
 * function that takes a function and turns it into a Promise.
 * (required when I want to pass a mixture of functions into asyncPipe for example)
 * Note: This should not be used for a real asynchronous function with a callback that you
 * need to wrap in a promise. that you should do manually.
 *
 * @export
 * @template ReturnValue
 * @param {(...args: any[]) => ReturnValue} func
 * @returns {(...args: any[]) => Promise<ReturnValue>}
 */
export function promisify<ReturnValue>(
  func: (...args: any[]) => ReturnValue
): (...args: any[]) => Promise<ReturnValue> {
  return function(...args: any[]) {
    return Promise.resolve(func(...args));
  };
}

export function stripKeyFromObject<ObjectType extends ObjectWithKeys>(
  key: string,
  object: ObjectType
): Partial<ObjectType> {
  const cloneOfObject = Object.assign({}, object);
  if (keyExistsOnObject(key, cloneOfObject)) {
    delete cloneOfObject[key];
  }
  return cloneOfObject;
}

export function stripKeyFromObjects<ObjectType extends ObjectWithKeys>(
  key: string,
  object: ObjectType[]
): Partial<ObjectType>[] {
  return object.map((obj: ObjectType) => stripKeyFromObject(key, obj));
}

export function getLastValueInArray<ObjectType>(
  values: ObjectType[]
): ObjectType | undefined {
  if (!Array.isArray(values)) return undefined;
  return values.slice(-1).pop();
}
export function upperCaseFirstLetter(value: string): string {
  if (isString(value) && value.length > 0) {
    const capitalStart = value[0].toUpperCase();
    return `${capitalStart}${value.substr(1)}`;
  }
  return value;
}
/**
 * takes an enum and returns an array of all its values...
 *
 * @export
 * @template enumType
 * @param {enumType} e
 * @returns {number[]}
 */
export function enumArrayifier<enumType>(e: enumType): number[] {
  return Object.keys(e)
    .map((key: string) => {
      return Number(key);
    })
    .filter((stateType: number) => !isNaN(stateType));
}

/**
 * no nice way of checking whether a string booleans, so wrote a super simple function to handle it
 *
 * @export
 * @param {string} bool
 * @returns {boolean}
 */
export function convertStringToBoolean(bool: string): boolean {
  if (typeof bool !== 'string') return false; // needs to be a string
  return bool.toLowerCase() === 'true'; // needs to be exactly true
}

/**
 * Random Helper functions that have no better place to live.
 */

export interface ICancelableES6Promise<T> extends Promise<T> {
  cancel: () => void;
  additionalData: any; // for storing any additional data...
}

/**
 * used when we need to dummy empty promises. borrowed from
 * http://rich.k3r.me/blog/2015/04/29/empty-promises-dos-and-donts-of-es6-promises/
 * @param val
 */
export function emptyPromise(): Promise<undefined> {
  return new Promise(resolve => {
    resolve(undefined);
  });
}

/**
 * Email Check. regex taken from https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript/1373724#1373724
 * by 'OregonTrail'. it is far from perfect, just simple validation. there is no perfect regex check for email.
 * still allow users that break this check to enter their email, they may have a weird email that is correct. instead, they should have a
 * 'test email' button to see if it works. use this email check just as a pop up warning that their email looks to be incorrect and they
 * should check it.
 *
 *
 * @param {String} email
 * @returns {boolean} valid
 */
export function emailCheck(email: string): boolean {
  const regex: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return regex.test(email);

  // if (typeof email !== 'string') return false;
  // if (email.length < 5) return false;

  // // If '@' is missing, or at index 0, return false
  // if (email.indexOf('@') < 1) return false;

  // // If '.' is missing, or at index 0, return false
  // if (email.indexOf('.') < 1) return false;
  // return true;
}
export type TTYpe =
  | 'undefined'
  | 'object'
  | 'boolean'
  | 'number'
  | 'string'
  | 'symbol'
  | 'function';

export function isType(type: TTYpe, value: any): boolean {
  return typeof value === type;
}

export const isString: (value: any) => boolean = partial(isType, 'string');

/**
 * https://stackoverflow.com/questions/4878756/how-to-capitalize-first-letter-of-each-word-like-a-2-word-city
 * @param str
 */
export function toTitleCase(str: string): string {
  if (!isString(str) || str.length === 0) return str;
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

export function isStringAndNotEmpty(str: string): boolean {
  return isString(str) && str.length > 0;
}

export function isAnUnEmptyArray(arr: any): boolean {
  return Array.isArray(arr) && arr.length > 0;
}

export function isAnUnEmptyArrayOfLength(arr: any, length: number): boolean {
  return Array.isArray(arr) && arr.length === length;
}

export function isItNullOrUndefined(value: any): boolean {
  if (value === null) return true;
  if (value === undefined) return true;
  return false;
}

export function isItNotNullOrNotUndefined(value: any): boolean {
  return !isItNullOrUndefined(value);
}

/**
 * function that takes matches that look like this:
 * ["match", undefined, "another match"]
 * and returns the actual amount of things we found - which is 2
 *
 * @export
 * @param {RegExpMatchArray} match
 * @returns {number}
 */
export function RegexMatchCount(match: RegExpMatchArray | null): number {
  if (match === undefined || match === null) return -1;
  // undefined's or nulls are falsy, and will get filtered out..
  return match.filter(val => val).length;
}

export interface IStringMatches {
  foundAMatch: boolean;
  matches: RegExpMatchArray | null;
}
export function stringMatches(
  stringToMatch: string,
  match: RegExp
): IStringMatches {
  const matches: RegExpMatchArray | null = stringToMatch.match(match);
  const foundAMatch: boolean = isAnUnEmptyArray(matches);
  return {
    foundAMatch,
    matches,
  };
}

/**
 *
 * splits an array into an arry of arrays of length 'smallerArraySize'
 *
 * @export
 * @template T
 * @param {T[]} array
 * @param {number} smallerArraySize
 * @returns {T[][]}
 */
export function chunkArray<T>(array: T[], smallerArraySize: number): T[][] {
  return array
    .map((value: T, index: number) => {
      return index % smallerArraySize === 0
        ? array.slice(index, index + smallerArraySize)
        : undefined;
    })
    .filter((value: T[] | undefined) => !isUndefined(value)) as T[][];
}
/**
 * useful for combining strings into smaller substring arrays
 * chunkString("test",2) => ["te","st"]
 * chunkString("tests",2) => ["te","st","s"]
 * @export
 * @param {string} str
 * @param {number} length
 * @returns {string[]}
 */
export function chunkString(
  str: string,
  length: number
): RegExpMatchArray | null {
  return str.match(new RegExp('.{1,' + length + '}', 'g'));
}

/**
 * returns byte[] for a given string of hexidecimal characters
 *
 * @param {string} hex
 * @returns {number[]}
 */
export function hexToBytes(hex: string): number[] {
  const chunks: RegExpMatchArray | null = chunkString(hex, 2);
  if (isNull(chunks)) return [];
  return chunks.map((byte: string) => {
    return parseInt(byte, 16);
  });
}

/**
 * https://stackoverflow.com/questions/14028148/convert-integer-array-to-string-at-javascript
 *
 * @export
 * @param {number[]} byteArray
 * @returns {string}
 */
export function decodeUTF8ByteArrayToUTF16String(byteArray: number[]): string {
  return decodeURIComponent(
    byteArray
      .map((value: number) => {
        return `%${('0' + value.toString(16)).slice(-2)}`; // the slice -2 is to make it only get the last two characters...so 042 becomes 42 which then becomes %42
      })
      .join('')
  );
}

export interface IEither<T> {
  reason: T;
  err: Error[];
}

export interface IEitherWithResult<T, R> extends IEither<T> {
  result: R;
}
