export * from './FP';
export * from './Lib';
// import * as R from 'ramda';

/**
 *
 *
 * @export
 * @interface ObjectWithKeys
 * @extends {Object}
 * @deprecated use Record instead
 */
export interface ObjectWithKeys extends Object {
  [key: string]: any;
}

/** from https://levelup.gitconnected.com/advanced-typescript-types-with-examples-1d144e4eda9e */
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

// crazy other types
//https://medium.freecodecamp.org/typescript-curry-ramda-types-f747e99744ab
export type Head<T extends any[]> = T extends [any, ...any[]] ? T[0] : never;
export type Tail<T extends any[]> = ((...t: T) => any) extends ((
  _: any,
  ...tail: infer TT
) => any)
  ? TT
  : [];

export type HasTail<T extends any[]> = T extends ([] | [any]) ? false : true;
export type FunctionInfer<F> = F extends (...args: infer A) => infer R
  ? [A, R]
  : never;

// gets the last value in an array or tuple
export type Last<T extends any[]> = {
  0: Last<Tail<T>>;
  1: Head<T>;
}[HasTail<T> extends true ? 0 : 1];

export type Length<T extends any[]> = T['length'];
export type Prepend<E, T extends any[]> = ((
  head: E,
  ...args: T
) => any) extends ((...args: infer U) => any)
  ? U
  : T;

export type Drop<N extends number, T extends any[], I extends any[] = []> = {
  0: Drop<N, Tail<T>, Prepend<any, I>>;
  1: T;
}[Length<I> extends N ? 1 : 0];

export type Cast<X, Y> = X extends Y ? X : Y;
export type Pos<I extends any[]> = Length<I>;
export type Next<I extends any[]> = Prepend<any, I>;
export type Prev<I extends any[]> = Tail<I>;
export type Iterator<
  Index extends number = 0,
  From extends any[] = [],
  I extends any[] = []
> = {
  0: Iterator<Index, Next<From>, Next<I>>;
  1: From;
}[Pos<I> extends Index ? 1 : 0];
export type Reverse<
  T extends any[],
  R extends any[] = [],
  I extends any[] = []
> = {
  0: Reverse<T, Prepend<T[Pos<I>], R>, Next<I>>;
  1: R;
}[Pos<I> extends Length<T> ? 1 : 0];
// export type Concat<T1 extends any[], T2 extends any[]> = Reverse<
//   Cast<Reverse<T1>, any[]>,
//   T2
// >;
// export type Append<E, T extends any[]> = Concat<T, [E]>;
// export type __ = typeof R.__;
// export type GapOf<
//   T1 extends any[],
//   T2 extends any[],
//   TN extends any[],
//   I extends any[]
// > = T1[Pos<I>] extends __ ? Append<T2[Pos<I>], TN> : TN;
// export type GapsOf<
//   T1 extends any[],
//   T2 extends any[],
//   TN extends any[] = [],
//   I extends any[] = []
// > = {
//   0: GapsOf<T1, T2, Cast<GapOf<T1, T2, TN, I>, any[]>, Next<I>>;
//   1: Concat<TN, Cast<Drop<Pos<I>, T2>, any[]>>;
// }[Pos<I> extends Length<T1> ? 1 : 0];
// export type PartialGaps<T extends any[]> = { [K in keyof T]?: T[K] | __ };
// export type CleanedGaps<T extends any[]> = {
//   [K in keyof T]: NonNullable<T[K]>
// };
// export type Gaps<T extends any[]> = CleanedGaps<PartialGaps<T>>;

// export type CurryV6<P extends any[], R> = <T extends any[]>(
//   ...args: Cast<T, Gaps<P>>
// ) => GapsOf<T, P> extends [any, ...any[]]
//   ? CurryV6<Cast<GapsOf<T, P>, any[]>, R>
//   : R;
// export type Curry<F extends (...args: any) => any> = <T extends any[]>(
//   ...args: Cast<Cast<T, Gaps<Parameters<F>>>, any[]>
// ) => GapsOf<T, Parameters<F>> extends [any, ...any[]]
//   ? Curry<(...args: Cast<GapsOf<T, Parameters<F>>, any[]>) => ReturnType<F>>
//   : ReturnType<F>;
